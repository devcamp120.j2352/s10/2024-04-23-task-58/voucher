package com.devcamp.voucher.controllers;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.voucher.entities.CVoucher;
import com.devcamp.voucher.services.CVoucherService;

@RestController
@RequestMapping("/api")
@CrossOrigin
public class CVoucherController {
    @Autowired
    private CVoucherService voucherService;

    @GetMapping("/vouchers")
    public ResponseEntity<ArrayList<CVoucher>> getAllVouchers() {
        ArrayList<CVoucher> cVouchers = this.voucherService.getAllVoucher();

        return new ResponseEntity<>(cVouchers, HttpStatus.OK);
    }
}
