package com.devcamp.voucher.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.voucher.entities.CVoucher;

public interface IVoucherRepository extends JpaRepository<CVoucher, Long> {    
}
